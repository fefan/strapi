FROM node:20.10-alpine AS builder

WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn

COPY ./ ./
RUN yarn build

ENTRYPOINT ["yarn", "strapi"]
CMD ["start"]
